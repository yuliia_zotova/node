const cors = require('cors');
const fs = require('fs');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');
const express = require('express');
const { Console } = require('console');
const { send } = require('process');
const app = express();
const port = 8080;
const fileDir = './files/';
const allowedExt = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(
  morgan('common', {
    stream: fs.createWriteStream('./access.log', { flags: 'a' })
  })
);

app.post('/api/files', (req, res, next) => {
  if (req.body.filename === undefined ) {
    res.status(500).send({ message: "Please specify 'filename' parameter" });
    next()
  } else {
    let fileName = req.body.filename
    fileName = req.body.filename.split('.');
    let fileNameCheck = allowedExt.find((str) => str === fileName[1]);
    if (fileName.length > 2) {
      fileNameCheck = allowedExt.find((str) => str === fileName[2]);
    }
    if (fileName.length === 1) {
      res.status(400).send({ message: "Please specify 'filename' parameter with correct extension" });
    } else if (fileNameCheck === undefined) {
      res.status(500).send({ message: 'Server error' });
    } else if (req.body.content === undefined) {
      res.status(400).send({ message: "Please specify 'content' parameter" });
    } else {
      fs.writeFile(path.join(fileDir, req.body.filename), req.body.content, (err) => {
        if (err) throw err;
      });
      res.status(200).send({ message: 'File created successfully' });
    }
  }
});

app.get('/api/files', (req, res) => {
  fs.readdir(fileDir, (err, files) => {
    if (files === undefined) {
      res.status(500).send({ message: 'Server error' });
    } else if (files.length === 0) {
      res.status(400).send({ message: 'Client error' });
    } else {
      res.status(200).send({ message: 'Success', files: files });
    }
  });
});

app.get('/api/files/:filename', (req, res) => {
  let fileName = req.params.filename;
  let filePath = path.join(fileDir, fileName);


  fs.readdir(fileDir, (err, files) => {
    let fileSearch = files.find((str) => str === fileName);
    if (files === undefined) {
      res.status(500).send({ message: 'Server error' });
    } else if (fileSearch === undefined) {
      res.status(400).send({ message: `No file with '${fileName}' filename found` });
    } else {
      fs.readFile(filePath, (err, data) => {
        const { birthtime } = fs.statSync(filePath);
          res.status(200).send({
            message: 'Success',
            filename: fileName,
            content: data.toString(),
            extension: path.extname(filePath).slice(1),
            uploadedDate: birthtime
          });
        }
      )};
});
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
